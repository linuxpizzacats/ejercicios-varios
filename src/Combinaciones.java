import java.util.ArrayList;

public class Combinaciones {


	public static ArrayList<String> getCombinaciones(int k) {
		
		int combinaciones=(int)Math.pow(2,k);
		ArrayList<String> retorno=new ArrayList<String>();
		for (int i = 0; i < combinaciones; i++) {
			retorno.add("");
		}
		for (int i = 0; i < k; i++) {
			setIntercalado(combinaciones,i,retorno);
		}
		
		return retorno;
	}
	public static void setIntercalado(int combinaciones,double posicionactual,ArrayList<String> array){
		int variacion=(int)Math.pow(2, posicionactual);
		int aux=0;
		for (int i = 0; i < combinaciones; i++) {		
			if(aux>=variacion*2) {
				aux=0;
			}
			if(aux<variacion) {
				aux++;
							
				array.set(i,"R".concat(array.get(i)));
			}else if(aux<variacion*2) {
				aux++;
				array.set(i, "N".concat(array.get(i)));
			}
		}
	}
	
}
