package JuegoPPT;

import java.util.Scanner;

public class Orquestador {
	private Juego juego;
	
	public Orquestador() {
		super();
		this.setJuego(new Juego());
	}

	public static void main(String[] args) {
		Orquestador orquestador= new Orquestador();
		orquestador.inicializarJuego();
		Scanner scanner=new Scanner(System.in);
		String seleccion="";
		
		while (!seleccion.equals("quit")) {
			orquestador.imprimirMenu();
			
			seleccion=scanner.next();
			orquestador.jugar(seleccion);

		}
		
		System.out.println("Juego Finalizado.");
		
		
	}
	
	private Juego getJuego() {
		return juego;
	}

	private void setJuego(Juego juego) {
		this.juego = juego;
	}

	private void inicializarJuego() {
		Elemento piedra= new Elemento("Piedra");
		Elemento papel= new Elemento("Papel");
		Elemento tijera=new Elemento("Tijera");
		
		// Piedra le gana a tijera y pierde con papel
		piedra.aEsteLeGano(tijera);
		piedra.conEstePierdo(papel);
		
		// Papel pierde con tijera
		papel.aEsteLeGano(piedra);
		papel.conEstePierdo(tijera);
		
		//tijera quedo definido en base a las anteriores definiciones
		tijera.aEsteLeGano(papel);
		tijera.conEstePierdo(piedra);
		
		this.getJuego().agregarElemento(piedra);
		this.getJuego().agregarElemento(papel);
		this.getJuego().agregarElemento(tijera);
		
	}
	
	private void imprimirMenu() {
		//this.limpiarPantalla();
		System.out.println("################## PIEDRA, PAPEL o TIJERA #######################");
		System.out.println("Seleccione una opción para jugar:");
		System.out.println("#################################################################");
		
		for (int j = 0; j < this.getJuego().getElementos().size(); j++) {
			System.out.println("\t "+j+" - "+this.getJuego().getElementos().get(j));
		}
		
		System.out.println("\t quit - Salir");
		System.out.println("#################################################################");
		System.out.print("Opción: ");
	}
	
	private void jugar(String seleccion) {
		try {
			int elegido=Integer.parseInt(seleccion);
			if(elegido>=0 && elegido < this.getJuego().getElementos().size()) {
				Elemento elemento= this.getJuego().getElementos().get(elegido);
				this.resultado(elemento);
			}
		} catch (Exception e) {
			
		}
	}
	
	private void resultado(Elemento elemento) {
		int resultado= this.getJuego().jugar(elemento);
		String mensaje;
		if(resultado==-1) {
			mensaje="¡PERDISTE!"; 
		}else if (resultado==1) {
			mensaje="¡GANASTE!";
		}else {
			mensaje="¡EMPATE!";
		}
		System.out.println(mensaje+"La computadora eligió "+this.getJuego().getElemento());
	}

	private void limpiarPantalla() {
	    System.out.print("\033[H\033[2J");  
	    System.out.flush();  
	}
}
