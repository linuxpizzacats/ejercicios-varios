package JuegoPPT;

import java.util.ArrayList;
import java.util.List;

public class Elemento {
	private List<Elemento> leGano;
	private List<Elemento> meGana;
	private String nombre;
	
	public Elemento() {
		super();
		this.setLeGano(new ArrayList<Elemento>());
		this.setMeGana(new ArrayList<Elemento>());
		this.setNombre("");
	}
	
	public Elemento(String nombre) {
		this();
		this.setNombre(nombre);
	}
	
	public String getNombre() {
		return nombre;
	}

	private void setNombre(String nombre) {
		this.nombre = nombre;
	}

	private List<Elemento> getLeGano() {
		return leGano;
	}

	private void setLeGano(ArrayList<Elemento> leGano) {
		this.leGano = leGano;
	}

	private List<Elemento> getMeGana() {
		return meGana;
	}

	private void setMeGana(ArrayList<Elemento> meGana) {
		this.meGana = meGana;
	}
	
	public boolean meGana(Elemento elemento) {
		return this.getMeGana().contains(elemento);
	}

	public boolean leGano(Elemento elemento) {
		return this.getLeGano().contains(elemento);
	}	
	
	public void aEsteLeGano(Elemento elemento) {
		if(!this.getLeGano().contains(elemento) && !this.getMeGana().contains(elemento)
				&& !this.equals(elemento)) {
			this.getLeGano().add(elemento);
			//elemento.conEstePierdo(this);
		}
	}
	
	public void conEstePierdo(Elemento elemento) {
		if(!this.getMeGana().contains(elemento) && !this.getLeGano().contains(elemento)
				&& !this.equals(elemento)) {
			this.getMeGana().add(elemento);
			//elemento.aEsteLeGano(this);
		}
	}
	@Override
	public String toString() {

		return this.getNombre();
	}
	
	@Override
	public boolean equals(Object obj) {

		return this.getNombre().equals(((Elemento)obj).getNombre());
	}

}
