package JuegoPPT;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Juego {
	private List<Elemento> elementos;
	private Elemento elemento;
	public Juego() {
		super();
		this.setElemento(null);
		this.setElementos(new ArrayList<Elemento>());
	}

	public Elemento getElemento() {
		return elemento;
	}

	private void setElemento(Elemento elemento) {
		this.elemento = elemento;
	}

	public List<Elemento> getElementos() {
		return elementos;
	}

	private void setElementos(List<Elemento> elementos) {
		this.elementos = elementos;
	}
	
	public void agregarElemento(Elemento elemento) {
		if(!this.getElementos().contains(elemento)) {
			this.getElementos().add(elemento);
		}
	}
	
	/*
	 * Elige un elemento de la lista al azar y evalua:
	 *  - Si le gana devuelve -1
	 *  - Si pierde devuelve 1
	 * 
	 * Por Defecto:
	 *  - Devuelve 0 (empate)
	 */
	public byte jugar(Elemento elemento) {
		byte retorno=0;
		Elemento elegidoPorNPC=this.getRandomElemento();
		this.setElemento(elegidoPorNPC);
		if(elegidoPorNPC.leGano(elemento)) {
			retorno=-1;
		}else if(elegidoPorNPC.meGana(elemento)) {
			retorno=1;
		}
		
		return retorno;
	}
	
	private Elemento getRandomElemento() {
		Random random=new Random();
		int maximo=this.getElementos().size();
		return this.getElementos().get(random.nextInt(maximo));
	}
	
}
