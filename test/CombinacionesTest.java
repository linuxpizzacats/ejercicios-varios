import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class CombinacionesTest {

	@Test
	public void combinacionesK2() {
		ArrayList<String> resultado=Combinaciones.getCombinaciones(2);
		assertTrue(resultado.contains("RR"));
		assertTrue(resultado.contains("RN"));
		assertTrue(resultado.contains("NR"));
		assertTrue(resultado.contains("NN"));
		assertEquals(4, resultado.size());
	}
	@Test
	public void combinacionesK3() {
		ArrayList<String> resultado=Combinaciones.getCombinaciones(3);
		assertTrue(resultado.contains("RRR"));
		assertTrue(resultado.contains("RRN"));
		assertTrue(resultado.contains("RNR"));
		assertTrue(resultado.contains("RNN"));
		assertTrue(resultado.contains("NRR"));
		assertTrue(resultado.contains("NRN"));
		assertTrue(resultado.contains("NNR"));
		assertTrue(resultado.contains("NNN"));
		assertEquals(8, resultado.size());
	}	
}
